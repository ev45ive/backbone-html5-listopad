"use strict";

var products = [{
  id: 1,
  name: 'JavaScript',
  price: 100,
  promotion: true,
  addedDate: new Date(2018, 11, 10),
}, {
  id: 2,
  name: 'BackBone',
  price: 60,
  promotion: false,
  addedDate: new Date(2018, 11, 9),
}]


var items = [{
  product: products[0],
  amount: 2
}, {
  product: products[1],
  amount: 1
}]


var productsView = new ProductsListView('.products .products-list', products)

productsView.render()

// ====

const cart = new Cart('Cart 1', items)

const cartView = new CartView('.cart-items', cart)
cartView.render()

cartView.$el.on('removeItem', function (e, item) {
  
  cart.removeFromCart(item.product)
  cartView.render()
})

productsView.$el.on('addToCart', function (event, data) {
  cart.addToCart(data)
  cartView.render()
})

// cart.on('update', cartView.render)