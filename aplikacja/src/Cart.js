function Cart(name, items) {

  this.name = name;
  this._items = items || [];
}
Cart.prototype = {

  getItems: function () {
    return this._items.map(function (item) {
      return {
        product: item.product,
        amount: item.amount,
        subtotal: this.getSubtotal(item.product, item.amount)
      }
    }.bind(this))
  },

  renderItems: function () {
    for (var i in this._items) {
      var item = items[i];
      this.renderItem(item)
    }
  },  

  addToCart: function (product, amount) {
    amount = amount || 1;

    var item = _.find(this._items, function (item) {
      return item.product.id == product.id
    })
    if (item) {
      item.amount += amount;
    } else {
      this._items.push({
        product: product,
        amount: amount
      })
    }
  },

  updateAmount(product, amount) {
    if (amount < 0) {
      return;
    }
    var item = _.find(this._items, function (item) {
      return item.product.id == product.id
    })
    if (item) {
      item.amount = amount
    }
  },

  removeFromCart(product) {
    this._items = _.filter(this._items, function (item) {
      return item.product.id !== product.id
    })
  },

  renderTotal: function () {
    return this._items.reduce(function (total, item) {
      var subtotal = this.getSubtotal(item.product, item.amount)
      return total += subtotal
    }.bind(this), 0)
  },

  getSubtotal: function (product, amount) {
    return amount * product.price
  },

  renderItem: function (item) {
    var product = item.product;
    var amount = item.amount;
    var subtotal = this.getSubtotal(product, amount)

    console.log(product.name +
      (product.promotion ? ' PROMOTION!' : '') +
      ' - ' +
      amount +
      ' x ' +
      product.price.toFixed(2) +
      ' = ' +
      subtotal.toFixed(2) +
      ' (' +
      (product.addedDate.getDate() + '-' + product.addedDate.getMonth() + '-' + product.addedDate.getFullYear()) +
      ') '
    )
  }
}