function ProductsListView(listSelector, products) {
  this.$el = $(listSelector)
  this.list = products || []

  this.$el.on('click', '.add-to-cart', function (event) {
    var btnElem = $(event.target);
    var itemElem = btnElem.closest('.product')
    this.onAddToCart(itemElem.data('product'))
  }.bind(this))
}

ProductsListView.prototype = {

  render: function () {
    this.$el.empty()
    // this.$el.append($.map(this.list, this.renderItem.bind(this)))

    $.each(this.list, function (index, product) {
      this.$el.append((this.renderItem(product)))

    }.bind(this))
  },

  onAddToCart: function (product) {
    this.$el.trigger('addToCart',product)
  },

  itemTpl: _.template('<li class="list-group-item product">' +
    '<span><%= name %> - <%= price.toFixed(2) %>' +
    '<input type="button" class="add-to-cart float-right" value="+ Add to cart">' +
    '</span></li>'),

  renderItem: function (product) {
    var elem = $(this.itemTpl(product))
        elem.data('product', product)
        
    return elem;

    // .on('click', '.add-to-cart', product, function (event) {
    //   // this.onAddToCart()
    //   console.log(event.data)
    // })
  }
}