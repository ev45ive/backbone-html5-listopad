function CartView(selector, cart) {
  this.$el = $(selector)
  this.cart = cart;
  this.$el.on('click', '.remove-item', function (event) {
    var item = $(event.target).closest('.cart-item').data('item')
    this.removeItem(item)
  }.bind(this))
}

CartView.prototype.render = function () {
  this.$el.empty()

  var items = this.cart.getItems()
  items.forEach(function (item) {

    this.$el.append(
      this.renderItem(item)
    )

  }.bind(this))
},

CartView.prototype.removeItem = function(item) {
  this.$el.trigger('removeItem', item)
}

CartView.prototype.renderItem = function (item) {
  return $('<tr class="cart-item">' +
    '<td>' + item.product.name + '</td>' +
    '<td>' + item.product.price + '</td>' +
    '<td>' + item.amount + '</td>' +
    '<td>' + item.subtotal.toFixed(2) + '</td>' +
    '<td><button class="close remove-item">&times;</button></td>' +
    '</tr>').data('item', item)
}