define(['backbone'], function (Backbone) {

  var TodoView = Backbone.View.extend({
    tagName: 'p',

    template: _.template(
      '<input type="checkbox" <%= completed? "checked":"" %>>' +
      '<span class="title"> <%= title %></span>' +
      '<span class="close">&times;</span>'
    ),

    events: {
      'change input[type=checkbox]': 'onCompleteClick',
      'click .close': 'onRemove'
    },

    onCompleteClick: function (event) {
      this.model.set('completed', event.target.checked)
    },

    onRemove: function () {
      this.trigger('remove', this.model)
    },

    initialize: function () {
      this.listenTo(this.model, 'change', this.render)
    },

    render: function () {
      this.$el.html(this.template(this.model.attributes))
      return this
    }
  })

  return TodoView
})