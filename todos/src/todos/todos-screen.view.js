define(['backbone',
  './todo.model',
  './todos.collection',
  './todos.view',
  './todo-editor.view',
], function (Backbone, Todo, Todos, TodosView, TodoEditorView) {
  var Backbone = require('backbone')

  var TodosScreenView = Backbone.View.extend({
    template: _.template('<div>' +
      '<h2>Todos</h2>' +
      '<div class="todos-list"></div>' +
      '<div class="todo-editor mt-2"></div>' +
      '</div>'),

    render: function () {
      this.$el.html(this.template())

      var todos = new Todos([{
        title: 'test',
        completed: true
      }, {
        title: 'test2',
        completed: true
      }], { /*  model:Todo, comparator: false */ })

      var todosView = new TodosView({
        el: this.$('.todos-list'),
        collection: todos
      }).render()

      var todo = new Todo({
        title: '',
        completed: false
      })

      var todoEditor = new TodoEditorView({
        el: this.$('.todo-editor'),
        // collection: todos,
        model: todo
      }).render()

      this.listenTo(todoEditor, 'addTodo', function (todo) {
        todos.add(todo)
      });
      // debugger
    }

  })

  return TodosScreenView;
})