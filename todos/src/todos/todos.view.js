define(['backbone',
  './todo.view'
], function (Backbone, TodoView) {

  var TodosView = Backbone.View.extend({
    template: _.template('<div>' +
      '<div class="list-group">' +
      '</div>' +
      '</div>'),

    initialize() {
      this.listenTo(this.collection, 'add', function (added) {
        this.addTodoItem(added)
      })
      this.listenTo(this.collection, 'remove', function (removed) {
        var view = this._views[removed.cid]
        view.remove()
      })
    },

    _views: {},

    addTodoItem: function (todo) {
      var itemView = new TodoView({
        model: todo,
        className: 'list-group-item'
      }).render()

      this.listenTo(itemView, 'remove', function (todo) {
        this.collection.remove(todo)
      })

      this.container.append(itemView.el)

      this._views[todo.cid] = itemView
    },

    render: function () {
      this.$el.html(this.template())
      this.container = this.$('.list-group')

      this.collection.each(function (todo) {
        this.addTodoItem(todo)
      }.bind(this))
    }

  })

  return TodosView;
})