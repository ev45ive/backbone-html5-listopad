define(['backbone'], function (Backbone) {

  var TodoEditorView = Backbone.View.extend({
    template: _.template('<div class="input-group">' +
      '<input class="form-control todo-title">' +
      '<button class="btn add-todo">Add Todo</button>' +
      '</div>'),

    events: {
      'click .add-todo': 'addTodo',
      'keyup .todo-title': 'onTitleEnterKey',
    },

    initialize: function () {
      this.listenTo(this.model, 'change', function () {
        this.$('.todo-title').val(this.model.get('title'))
        this.$('.add-todo').val(this.model.get('completed'))
      })
    },

    addTodo: function () {
      this.updateTodo();
      this.trigger('addTodo', this.model.toJSON())
      // this.collection.add(this.model.toJSON())
      this.model.set('title', '')
    },

    updateTodo: function () {
      this.model.set('title', this.$('.todo-title').val())
    },

    onTitleEnterKey(event) {
      if (event.keyCode == 13) {
        this.addTodo()
      }
    },

    render: function () {
      this.$el.html(this.template())
      return this
    }
  })

  return TodoEditorView;
})