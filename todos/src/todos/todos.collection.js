define(['backbone',
  './todo.model',
], function (Backbone, Todo) {

  var Todos = Backbone.Collection.extend({
    model: Todo
  })

  return Todos
})