define([
  'backbone.marionette',
  './users/users-screen.view'
], function (Marionette, UsersScreen) {

  Marionette.View.setRenderer(function (template, data) {
    return _.template(template)(data);
  });
  
  Marionette.CollectionView.setRenderer(function (template, data) {
    return _.template(template)(data);
  });

  var App = Marionette.Application.extend({

    onStart: function () {

      this.showView(new UsersScreen())
    }
  })


  return App;
})