define([
  'backbone',
  'backbone.marionette'
], function (Backbone, Marionette) {

  var MapView = Marionette.View.extend({
    template: '<img>',
    model: new Backbone.Model({
      lat: "-37.3159",
      lng: "81.1496",
      zoom: 13,
      width: 300,
      height: 100,
      // https://jsfiddle.net/u2x98dsy/1/
      key: 'AIzaSyDouxOM4EQmVjhbsdWcGPE9CNt6_M6XdVk'
    }),

    ui: {
      map: 'img'
    },

    events: {
      'click @ui.map': function () {

        navigator.geolocation.getCurrentPosition(resp => {
          this.model.set({
            lat: resp.coords.latitude,
            lng: resp.coords.longitude
          })
        })
      }
    },

    modelEvents: {
      'change': 'updateMap'
    },

    updateMap: function () {
      this.ui.map.attr('src', 'https://maps.googleapis.com/maps/api/staticmap?' +
        'center=' + this.model.get('lat') + ',' + this.model.get('lng') +
        '&zoom=' + this.model.get('zoom') +
        '&size=' + this.model.get('width') + 'x' + this.model.get('height') +
        '&key=' + this.model.get('key') +
        '&markers=' + 'color:red|label|' + this.model.get('lat') + ',' + this.model.get('lng')
      )
    }

  })

  return MapView
})
// https://maps.googleapis.com/maps/api/staticmap?
// center=52.2,21&zoom=13&size=600x300&maptype=roadmap&markers=color:red%7Clabel:C%7C52.2,21&key=AIzaSyDouxOM4EQmVjhbsdWcGPE9CNt6_M6XdVk