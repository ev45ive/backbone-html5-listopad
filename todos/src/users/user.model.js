define([
  'backbone'
], function (Backbone) {

  var UserModel = Backbone.Model.extend({
    // url: function () {
    //   return 'http://localhost:3000/users/' + this.id
    // },

    defaults: {
      name: '',
      email: '',
    },

    validate: function (attrs, options) {

      if (attrs.name.length < 3) {
        return "Name too short"
      }
    }
  })

  return UserModel;

})