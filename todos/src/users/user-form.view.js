define([
  'backbone.marionette',
  '../shared/map.view'
  // 'tpl!user-form.view.tpl.html'
], function (Marionette, MapView) {

  var UserFormView = Marionette.View.extend({
    template: '<form>' +
      '<h3>Edit User</h3>' +
      '<div class="form-group"> <label>Name:</label>' +
      ' <input class="form-control" value="<%= name %>" name="name"> ' +
      '<div class="name-errors"></div>' +
      '</div>' +
      '<div class="form-group"> <label>Email:</label> ' +
      ' <input class="form-control" value="<%= email %>" name="email"> ' +
      // Upload
      '<div class="form-group mt-2"> <label>Upload Avatar:</label> ' +
      ' <input type="file" name="avatar"> ' +
      ' <img class="avatar-preview" width="50">' +
      '</div>' +
      //Map:
      '<div class="map"></div>' +
      '<input type="button" class="action-cancel btn btn-danger" value="Cancel">' +
      '<input type="button" class="action-save btn btn-success" value="Save">' +
      '</form>',

    ui: {
      nameField: 'input[name=name]',
      nameErrors: '.name-errors',
      emailField: 'input[name=email]',
      avatarField: 'input[name=avatar]',
      avatar: '.avatar-preview',
      cancelBtn: '.action-cancel',
      saveBtn: '.action-save',
    },

    regions: {
      map: '.map'
    },

    onRender: function () {
      var mapView = new MapView()
      this.showChildView('map', mapView);

      mapView.model.set(
        this.model.get('address').geo
      )
      mapView.updateMap()

    },

    events: {
      'keyup @ui.nameField': 'nameChange',
      'keyup @ui.emailField': 'emailChange',
      'change @ui.avatarField': 'avatarChange',
    },

    triggers: {
      'click @ui.cancelBtn': 'form:cancel',
      'click @ui.saveBtn': 'form:save',
    },

    modelEvents: {
      'invalid change': function (model) {
        this.ui.nameErrors.text(model.validationError)
      },
    },

    avatarChange: function (event) {
      var files = this.ui.avatarField.prop('files')
      var url = URL.createObjectURL(files[0])
      this.ui.avatar.attr('src', url)
    },

    nameChange: function (e) {
      this.model.set('name', $(e.target).val(), {
        validate: true
      })
    },

    emailChange: function (e) {
      this.model.set('email', $(e.target).val())
    },

    // cancel: function () {

    // },
    // save: function () {
    //    this.trigger('form:save', this)
    // },

  })

  return UserFormView

})