define([
  'backbone.marionette',
  'backbone',
  './users.collection',
  './user-detail.view',
  './user-form.view',
  './users-list.view',
], function (Marionette, Backbone, UsersCollection, DetailView, UserFormView, UsersListView) {


  return Marionette.View.extend({

    template: '<div class="row">' +
      '<div class="col users-list">a</div>' +
      '<div class="col user-detail">Please select user</div>' +
      '</div>',

    regions: {
      list: '.users-list',
      detail: '.user-detail'
    },

    initialize: function () {
      this.userList = new UsersCollection([])
      console.log(this.userList)
      this.userList.fetch()
    },

    onRender: function () {

      this.showChildView('detail', new Marionette.View({
        template: 'Please select user'
      }));

      this.showChildView('list', new UsersListView({
        collection: this.userList
      }))
    },

    childViewEvents: {
      'cancel:click': 'onChildviewCancelClick',
      'user:selected': 'onSelectUser',
      'form:save': 'onFormSave'
    },

    onFormSave: function (view) {
      // view.model.save({}, {
      //   success: function (model, response, options) {
      //     this.userList.fetch()
      //   }.bind(this)
      // })
      if (!view.model.isValid()) {
        return
      }
      this.userList.set(view.model, {
        add: false,
        remove: false
      })
      this.userList.get(view.model.id).save()
    },

    onSelectUser: function (selectedId) {
      var user = this.userList.get(selectedId)

      this.showChildView('detail', new UserFormView({
        model: user.clone()
      }));
    },

    // childview:cancel:click
    onChildviewCancelClick: function (e) {
      this.getChildView('detail').remove()
    }

  })

})