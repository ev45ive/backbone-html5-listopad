define([
  'backbone.marionette',
  "./users-list-item.view"
], function (Marionette, UserItemView) {

  var UsersListView = Marionette.CollectionView.extend({
    template: '<div>' +
      '<h3>Users</h3>' +
      '<div class="list-group"></div>' +
      '</div>',
    childViewContainer: '.list-group',
    childView: UserItemView,

    model: new Backbone.Model({
      selectedId: null
    }),

    modelEvents: {
      'change:selectedId': function (model, selectedId) {
        // this.$('.')
        // console.log(arguments)
        // console.log(this.children.findByModelCid(selectedId))

        this.children.each(function (childView) {
          if (childView.model.cid == selectedId) {
            childView.setSelected(true)
          } else {
            childView.setSelected(false)
          }
        })

        this.trigger('user:selected', selectedId)
      }
    },

    // childViewTriggers:{

    // },

    childViewEvents: {
      'item:select': 'onChildviewItemSelect'
    },

    onChildviewItemSelect(view) {
      this.model.set('selectedId', view.model.cid)
    }
  })

  return UsersListView;
})