define([
  'backbone',
  './user.model'
], function (Backbone, UserModel) {

  var UsersCollection = Backbone.Collection.extend({
    // url: 'http://localhost:3000/users',
    url: 'http://10.0.3.246:3000/users',
    model: UserModel
  })

  return UsersCollection;

})