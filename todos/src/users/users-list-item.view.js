define([
  'backbone.marionette'
], function (Marionette) {

  var UserItemView = Marionette.View.extend({
    // tagName:'li',
    className: 'list-group-item list-group-item-action',
    template: '<span class="name"><%= name %></span>',

    triggers: {
      'click': 'item:select'
    },

    modelEvents: {
      'change:name': 'updateName'
    },

    updateName: function () {
      this.$('.name').text(this.model.get('name'))
    },

    setSelected(isActive) {
      this.$el.toggleClass('active', isActive)
    },

    onItemSelect() {
      // this.$el.toggleClass('active')
    },

  })

  return UserItemView

})