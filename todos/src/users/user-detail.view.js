define([
  'backbone.marionette'
], function (Marionette) {
  // https://www.npmjs.com/package/requirejs-underscore-tpl

  var DetailView = Marionette.View.extend({
    template: '<div>' +
      '<dl>' +
      '<dt>Name:</dt><dd><%= name %></dd>' +
      '<dt>Email:</dt><dd><%= email %></dd>' +
      '</dl>' +
      '<button class="cancel">Cancel</button>' +
      '</div>',

    // events:{
    //   'click button': 'onButtonClick'
    // }
    // onButtonClick(){
    //   this.trigger('placki')
    // }

    triggers: {
      'click .cancel': 'cancel:click'
    },

    onCancelClick(e){
      // console.log('test',e)
    }

  })

  return DetailView;
})