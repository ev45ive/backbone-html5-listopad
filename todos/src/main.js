define([
  './app'
], function (App) {

  var app = new App({
    region: '#app-root'
  })
  app.start()

})